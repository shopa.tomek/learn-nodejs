const url = require('url');
const { serialize } = require('v8');

const myUrl = new URL('http://myweb.com/hello.html?id=100&status=active');

// serialized url

console.log(myUrl.href);
console.log(myUrl.toString());

// host root domain

console.log(myUrl.host);
console.log(myUrl.hostname);

//path name

console.log(myUrl.pathname);

//serializer query

console.log(myUrl.search);
console.log(myUrl.searchParams);

//add params

myUrl.searchParams.append('abc', '123');
console.log(myUrl.searchParams);





