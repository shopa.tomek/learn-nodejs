// module wrapper function
// (function(exports, require, module, __filename, __dirname){
// )}

// console.log(__filename, __dirname);


class Man {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
  greeting() {
    console.log(`my name is ${this.name} and i am ${this.age}`);
  }
}


module.exports = Man;
